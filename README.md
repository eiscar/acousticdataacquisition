# Acoustic Data Acquisition and Processing #

## Data folder ## 

1. Amplifier_Transponder_Characteristics.csv ---> Custom Amplifier response using AS-1 as reference hydrophone
2. Receiver.bod --> Bode plot generated with MultiSim for the Receiver amplifier circuit board 
3. Transmitter.bod --> Bode plot generated with MultiSim for the Transmitter amplifier circuit board

## Scripts ## 

1. bode_plotter.py --> Script to plot .bod files from the data folder
2. main.py --> Connect to the STM-32 receiver board through serial and load the acquired data
3. SensitivityPlotter.py --> Script to plot the sensitivity .csv file from the data folder 
4. signal_processing.py --> SDFT and ZeroCrossing Signal Processing functions