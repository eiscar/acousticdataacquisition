
#!/usr/bin/python

from __future__ import division
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot  as pyplot
import os
#File to generate bode plots for the transmitter and receiver opamp circuits.

if __name__=="__main__":

    current_path = os.path.dirname(os.path.realpath(__file__))

    #relpath = "./data/Transmitter.bod"
    relpath = "./data/Receiver.bod"

    receiver_file = os.path.join(current_path, relpath)

    data = np.loadtxt(receiver_file, skiprows=15)
    print(data)
    labelalignment = -0.07

    fig = pyplot.figure()
    ax = fig.add_subplot(2,1,1)
    ax.grid(True, which="both", ls="-")
    line, = ax.plot(data[:, 0], data[:, 1], color='blue', lw=2)
    #ax.set_title('Receiver Amplifier Bode Plot', size=20)
    ax.set_xscale('log')
    ax.set_xlim(10**3,10**7)
    ax.set_ylim(-100,50)
    ax.yaxis.set_label_coords(labelalignment, 0.5)

    ax.set_ylabel('Gain [dB]',size=20)
    ax2 = fig.add_subplot(2, 1, 2)
    line2, = ax2.plot(data[:, 0], data[:, 2], color='blue', lw=2)
    ax2.set_xscale('log')
    ax2.set_ylabel('Phase [deg]',size=20)
    ax2.set_xlabel('Frequency [Hz]',size=20)
    ax2.grid(True,which="both",ls="-")
    ax2.set_xlim(10**3,10**7)
    ax2.yaxis.set_label_coords(labelalignment, 0.5)
    print(max(data[:,1]))
    pyplot.show()