
#!/usr/bin/python

from __future__ import division
import numpy as np
import matplotlib.pyplot  as pyplot
import os
import pandas as pd
#File to generate bode plots for the transmitter and receiver opamp circuits.

if __name__=="__main__":

    current_path = os.path.dirname(os.path.realpath(__file__))

    #relpath = "./data/Transmitter.bod"
    relpath = "./data/Amplifier_Transponder_Characteristics.csv"

    receiver_file = os.path.join(current_path, relpath)
    s = open(receiver_file).read().replace(',','.')
    #data = np.loadtxt(StringIO.StringIO(s), skiprows=6,delimiter=';', )
    data = pd.read_csv(receiver_file,skiprows=5, delimiter=';',decimal=',').as_matrix()
    print(data[:, 0])
    labelalignment = -0.07

    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.grid(True, which="both", ls="-")
    line, = ax.plot(data[:, 0], data[:, 11], color='blue', lw=2)
    #ax.set_title('Receiver Amplifier Bode Plot', size=20)
#    ax.set_xlim(10**3,10**7)
 #   ax.set_ylim(-100,50)
    ax.yaxis.set_label_coords(labelalignment, 0.5)
    ax.set_xticks(np.arange(min(data[:,0]), max(data[:,0])+1, 1.0))
    ax.set_ylabel('Sensitivity [dB]',size=20)
    ax.set_xlabel('Frequency [kHz]',size=20)

    pyplot.show()