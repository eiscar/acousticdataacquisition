#!/usr/bin/python
from __future__ import division
import numpy as np
import matplotlib.pyplot  as pyplot
import os
from signal_processing import detect_peaks
import acoustic_utilities as au
#File to generate bode plots for the transmitter and receiver opamp circuits.

def remove_units(line):
    line.split(' ')
    modline = line.replace('kHz', '').replace(' ','').replace('Z','').replace('\xa3[\r\n','').split(':')[1:3]
    try:
        if 'k' in modline[1]:
            modline[1]=float(modline[1].replace('k',''))*1000
    except:
        pass
    return modline


def plot_impedance():
    current_path = os.path.dirname(os.path.realpath(__file__))

    #relpath = "./data/Transmitter.bod"
    relpath = "./data/DL-2-AIR-Impedance.TXT"

    receiver_file = os.path.join(current_path, relpath)
    freq = []
    impedance = []
    with open(receiver_file, 'rb') as f:
        lines = map(remove_units, f)
        for line in lines[12:]:
            freq.append(float(line[0]))
            impedance.append(float(line[1]))

    data = np.column_stack((freq, impedance))
    labelalignment = -0.07

    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.grid(True, which="both", ls="-")
    line, = ax.plot(data[:, 0], data[:, 1], color='blue', lw=2)

    res_freq = data[ np.argmin(data[:,1]), 0]
    antires_freq = data[detect_peaks(data[:,1]),0][0]
    Zmin = data[detect_peaks(1/data[:,1]),1][0]

    print 'Resonant freq: ', res_freq
    print 'AntiResonant freq: ', antires_freq
    print 'Minimum Impedance:', Zmin

    ax.yaxis.set_label_coords(labelalignment, 0.5)
    ax.set_xticks(np.arange(min(data[:,0]), max(data[:,0])+1, 2.0))
    ax.set_ylabel('Impedance [Ohm]',size=20)
    ax.set_xlabel('Frequency [kHz]',size=20)

    #pyplot.show()



if __name__=="__main__":

    plot_impedance()
    eqcircuit = au.compute_equivalent_circuit(38670, 42670, 6.375*10**-9, 377.4, 0.005)

    print 'Effective dynamic coupling coefficient (squared)', eqcircuit['keff2']
    print 'Clamped capacitance', eqcircuit['C0']
    print 'Electrical equivalent inductor:  ', eqcircuit['Le']
    print 'Electrical equivalent capacitor: ', eqcircuit['Ce']
    print 'Electrical equivalent resistance ', eqcircuit['Re']
    print 'Electrical dissipation factor R0:', eqcircuit['R0']
    print ' '
    print 'Electrical quality factor Qe:    ', eqcircuit['Qe']
    print 'Parallel tuning inductor:        ', eqcircuit['L_tuning_parallel']
    print 'Series tuning inductor:          ', eqcircuit['L_tuning_series']