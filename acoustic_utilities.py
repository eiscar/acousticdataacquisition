from __future__ import division, print_function
from math import exp, pow, log10, pi, sqrt
import numpy as np

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


def absorption_coefficient(T,f):
    """
    Computes the absortion coefficient as a function of temperature and frequency according
    to Fischer and Simmons (1977)
    :param T: Temperature in degree Celsius
    :param f: Frequency in Hz
    :return: absorption coefficient in dB/m
    """
    A2 = 48.8*(10**-8)+65.4*(10**-10)*T
    f2 = 1.6*10**7*(T+273)*exp(-3052/(T+273))
    alfa = (A2*f2*pow(f, 2))/(pow(f2, 2)+pow(f, 2))
    return alfa


def spl_from_receiver(RVS, G, Vout):
    """
    Computes the sound pressure intensity at the receiver from the observed voltage
    :param RVS:  Free field voltage sensitivity dB re 1V/1uPa
    :param G:    Amplifier gain in dB the hydrophone is connected to
    :param Vout: RMS Voltage read at the output of the amplifier
    :return:     Sound Pressure Level (SPL) in dB
    """

    SPL = abs(RVS) - G + 20*log10(Vout)
    return SPL


def voltage_at_reveiver(SPL, RVS):
    """
    Compute the hydrophone voltage output (without amplifier) for a given sound pressure level (SPL)
    :param SPL:  Sound Pressure Level (SPL) in dB re 1uPa
    :param RVS: Receive Voltage sensitivity
    :return: Voltage for a given SPL and RVS
    """

    Vout = pow(10, (SPL-abs(RVS))/20)
    return Vout


def sl_from_receiver(SPL, alfa, R):
    """
    Computes the source level given a sound pressure level at a given distance R from the source
    :param SPL:  Sound Pressure Level (SPL) in dB
    :param alfa: Absorption coefficient in dB/m
    :param R:    Distance from Source to Receiver in m
    :return:     Source Level (SL) in dB
    """
    SL = SPL + transmission_loss(R, alfa)
    return SL


def spl_from_transmitter(SL, alfa, R):
    """
    Computes the source level given a sound pressure level at a given distance R from the source
    :param SL:   Source  Level (SL) in dB
    :param alfa: Absorption coefficient in dB/m
    :param R:    Distance from Source to Receiver in m
    :return:     Source Level (SPL)
    """
    SPL = SL - transmission_loss(R, alfa)
    return SPL


def transmission_loss(R, alfa):
    """
    Computes the transmission loss
    :param R:       Distance in m
    :param alfa:    Absorption coeficient
    :return:  Transmission loss (TL) in dB
    """

    TL = 20*log10(R) + alfa*R
    return TL


def sl_from_transmitter(TVR, Vin):
    """
    Computes the signal source level (SL) as a function of the emitters transmit voltage response (TVR) and amplifier
    gain
    :param TVR: transmit voltage response of the emitting hydrophone in dB re 1uPa/V
    :param Vin: RMS Voltage applied to the transducer
    :return:  Source level (SL) in dB
    """
    SL = TVR + 20*log10(Vin)
    return SL


def compute_equivalent_circuit(f_res, f_antires, Cf, Zmin, tand):
    """
    Computes the values of the Van Dyke equivalent circuit according to chapter 12 of:
    'Transducers and Arrays for Underwater Sound"
    :param f_res:     Resonant frequency in Hz
    :param f_antires: Antiresonance frequency in Hz
    :param Cf:        Free capacitance in F measured at 1kHz
    :param Zmin:      Minimum impedance in Ohm
    :param tand:      Dissipation factor measured at 1kHz
    :return:          Equivalent circuit parameters:
                      Effective dynamic coupling coefficient (squared) keff2
                      Clamped capacitance', C0 in F
                      Electrical equivalent inductor:  Le in H
                      Electrical equivalent capacitor:  Ce in F
                      Electrical equivalent resistance: Re in F
                      Electrical dissipation factor: R0 in Ohm
                      Electrical quality factor: Qe dimensionless
                      Parallel tuning inductor:  L_tuning_parallel in H
                      Series tuning inductor:  L_tuning_series in H
    """

    eq_circ = dict()
    w = 2*pi*1000  #Cf and tand frequency
    wr = 2*pi*f_res
    eq_circ['keff2'] = 1-(f_res/f_antires)**2
    eq_circ['C0'] = Cf *(1-eq_circ['keff2'])
    eq_circ['Ce'] = Cf * eq_circ['keff2']
    eq_circ['Le'] = 1/((wr**2)*eq_circ['C0'])
    eq_circ['Re'] = Zmin
    eq_circ['R0'] = 1/(w*Cf)*tand

    # Tuning Inductors
    eq_circ['L_tuning_parallel'] = 1/(wr**2 * eq_circ['C0'])   # No effect on transmitter
    eq_circ['Qe'] = (wr*eq_circ['C0']*eq_circ['R0']*eq_circ['Re'])/(eq_circ['R0']+eq_circ['Re'])
    # No effect on receiver
    eq_circ['L_tuning_series'] = (eq_circ['L_tuning_parallel']* eq_circ['Qe']**2)/(1+ eq_circ['Qe']**2)
    return eq_circ


def spl_vs_range(Vin, TVR, f, max_r, steps=1000):
    SL = sl_from_transmitter(TVR,Vin)
    alfa = absorption_coefficient(15, f)

    R = np.linspace(0.1, max_r, steps)
    spl = np.array(map(lambda x: spl_from_transmitter(SL, alfa, x), R))

    return R, spl


def speed_of_sound(T, S, P):
    """
    Computation of speed of sound according to Chen and Milero (1977).
    Valid for 0<T<40 0<S<40 0<P<1e+8
    Code adapted from http://resource.npl.co.uk/acoustics/techguides/soundseawater/unesco.html
    :param T: Temperature in degree Celsius
    :param S: Salinity in parts per thousend
    :param P: pressure in Pa
    :return:  Speed of sound (c) in m/s
    """

    C00 = 1402.388
    C01 = 5.03830
    C02 = -5.81090E-2
    C03 = 3.3432E-4
    C04 = -1.47797E-6
    C05 = 3.1419E-9
    C10 = 0.153563
    C11 = 6.8999E-4
    C12 = -8.1829E-6
    C13 = 1.3632E-7
    C14 = -6.1260E-10
    C20 = 3.1260E-5
    C21 = -1.7111E-6
    C22 = 2.5986E-8
    C23 = -2.5353E-10
    C24 = 1.0415E-12
    C30 = -9.7729E-9
    C31 = 3.8513E-10
    C32 = -2.3654E-12
    A00 = 1.389
    A01 = -1.262E-2
    A02 = 7.166E-5
    A03 = 2.008E-6
    A04 = -3.21E-8
    A10 = 9.4742E-5
    A11 = -1.2583E-5
    A12 = -6.4928E-8
    A13 = 1.0515E-8
    A14 = -2.0142E-10
    A20 = -3.9064E-7
    A21 = 9.1061E-9
    A22 = -1.6009E-10
    A23 = 7.994E-12
    A30 = 1.100E-10
    A31 = 6.651E-12
    A32 = -3.391E-13
    B00 = -1.922E-2
    B01 = -4.42E-5
    B10 = 7.3637E-5
    B11 = 1.7950E-7
    D00 = 1.727E-3
    D10 = -7.9836E-6

    p = P*0.00001

    Cw = (C00 + C01 * T + C02 * pow(T,2) + C03 * pow(T, 3) + C04 * pow(T, 4) +
    C05 * pow(T, 5)) + (C10 + C11 * T + C12 * pow(T, 2) + C13 * pow(T, 3) +
    C14 * pow(T, 4)) * p + (C20 + C21 * T + C22 * pow(T,2) +
    C23 * pow(T, 3) + C24 * pow(T, 4)) * pow(p,2) + (C30 + C31 * T + C32 * pow(T, 2)) * pow(p, 3)

    A = (A00 + A01 * T + A02 * pow(T, 2) + A03 * pow(T,3) + A04 * pow(T, 4)) + \
    (A10 + A11 * T + A12 * pow(T, 2) + A13 * pow(T, 3) + A14 * pow(T, 4)) * p + \
    (A20 + A21 * T + A22 * pow(T, 2) + A23 * pow(T, 3)) * pow(p, 2)\
    + ( A30 + A31 * T + A32 * pow(T, 2)) * pow(p, 3)
    B = B00 + B01 * T + (B10 + B11 * T) * p
    D = D00 + (D10 * p)

    C = Cw + A * S + B * pow(S, (3 / 2)) + D * pow(S,2)

    return C

if __name__=='__main__':
    #distance, spl_vec = spl_vs_range(25, 134, 35000, 3000)
    #plt.plot(distance,spl_vec)
    #print(voltage_at_reveiver(60,-190))
    #plt.show()
    AMP_GAIN = 44 # dB
    SL = sl_from_transmitter(120,25*sqrt(2)/2)
    alfa = absorption_coefficient(15, 41700)
    SPL = spl_from_transmitter(SL, alfa, 6)
    V = voltage_at_reveiver(SPL+AMP_GAIN,-190)

    print('Voltage at output of receiver: ', 2*V/sqrt(2),'Vpp')
