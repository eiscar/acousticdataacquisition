from __future__ import division

import matplotlib.pyplot as plot
import numpy as np
from scipy import signal
import math
import cmath

def load_signal():
    data = np.load("/home/eiscar/PyCharm_Projects/RigolAdquisition/data.npy")
    return data

def scipy_spectrogram(data, fs):
    print "Processing Spectrogram "

    rec_signal = data[0,:] - np.average(data[0,:])
    points = 16384
    f, t, Sxx = signal.spectrogram(rec_signal, fs, window='hamming', nperseg=points, noverlap=0.5*points, nfft=4*points,
    detrend=False, return_onesided=True, scaling='density' )

    plot.pcolormesh(t, f, Sxx)
    plot.ylabel('Frequency [Hz]')
    plot.xlabel('Time [sec]')
    plot.show()


def plot_data(data):

    plot.plot(data[3,:], data[0,:], data[3,:], data[1, :], data[3, :], data[2,:])
    plot.show()


def custom_spectogram(data):
    data = data[1,:] - np.average(data[1,:])

    fs = 25000000
    fft_size = 65536
    win_size = 32768
    overlap_fac = 0.8

    hop_size = np.int32(np.floor(win_size * overlap_fac))
    total_segments = np.int32(np.ceil(len(data) / np.float32(hop_size)))
    #Todo: changing the overlap factor doens't seem to preserve energy, need to fix this
    window = np.hanning(win_size) * overlap_fac * 2
    inner_pad = np.zeros((fft_size * 2) - win_size)

    proc = np.concatenate((data, np.zeros(fft_size)))
    result = np.empty((total_segments, fft_size), dtype=np.float32)

    for i in xrange(total_segments):
        current_hop = hop_size * i
        segment = proc[current_hop:current_hop + win_size]
        windowed = segment * window
        padded = np.append(windowed, inner_pad)
        spectrum = np.fft.fft(padded) / fft_size
        autopower = np.abs(spectrum * np.conj(spectrum))
        result[i, :] = autopower[:fft_size]

    result = 20*np.log10(result)          # scale to db
    #result = np.clip(result, -40, 200)   # clip values

    img = plot.imshow(result, origin='lower', cmap='jet', interpolation='nearest', aspect='auto')
    plot.show()

def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    """Detect peaks in data based on their amplitude and other features.

    Parameters
    ----------
    x : 1D array_like
        data.
    mph : {None, number}, optional (default = None)
        detect peaks that are greater than minimum peak height.
    mpd : positive integer, optional (default = 1)
        detect peaks that are at least separated by minimum peak distance (in
        number of data).
    threshold : positive number, optional (default = 0)
        detect peaks (valleys) that are greater (smaller) than `threshold`
        in relation to their immediate neighbors.
    edge : {None, 'rising', 'falling', 'both'}, optional (default = 'rising')
        for a flat peak, keep only the rising edge ('rising'), only the
        falling edge ('falling'), both edges ('both'), or don't detect a
        flat peak (None).
    kpsh : bool, optional (default = False)
        keep peaks with same height even if they are closer than `mpd`.
    valley : bool, optional (default = False)
        if True (1), detect valleys (local minima) instead of peaks.
    show : bool, optional (default = False)
        if True (1), plot data in matplotlib figure.
    ax : a matplotlib.axes.Axes instance, optional (default = None).

    Returns
    -------
    ind : 1D array_like
        indeces of the peaks in `x`.

    Notes
    -----
    The detection of valleys instead of peaks is performed internally by simply
    negating the data: `ind_valleys = detect_peaks(-x)`

    The function can handle NaN's

    See this IPython Notebook [1]_.

    References
    ----------
    .. [1] http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb

    """

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        if ax is None:
            _, ax = plot.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if ind.size:
            label = 'valley' if valley else 'peak'
            label = label + 's' if ind.size > 1 else label
            ax.plot(ind, x[ind], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (ind.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        mode = 'Valley detection' if valley else 'Peak detection'
        ax.set_title("%s (mph=%s, mpd=%d, threshold=%s, edge='%s')"
                     % (mode, str(mph), mpd, str(threshold), edge))
        # plt.grid()
        plot.show()

    return ind

def process_peaks(peak_ind):

    peak_diff = [0]
    prev_peak = 0
    for peak in peak_ind:
        peak_diff.append(peak-prev_peak)
        prev_peak = peak

    plot.plot(peak_diff)
    plot.show()


def detect_zero_crossings(x, show=False, ax=None):
    zero_crossings = np.where(np.diff(np.sign(x)))[0]

    if show:
        if ax is None:
                _, ax = plot.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if zero_crossings.size:
            label = 'Zeros'
            label = label + 's' if zero_crossings.size > 1 else label
            ax.plot(zero_crossings, x[zero_crossings], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (zero_crossings.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        ax.set_title("Zero Crossings ")
        # plt.grid()
        plot.show()
    return zero_crossings

def sdft(x, f0, fs, window):

    '''

    :param x: Data Sequence to analyze
    :param f0: Frequency of interest in Hz
    :param fs: Sampling frequency in Hz
    :param window: Window Size
    :return: SDFT array
    '''

    k = (f0*window)/fs
    n = int(math.ceil(k))

    Xk = np.zeros(len(x), dtype=np.complex_)

    for m in range(window):
        Xk[n] += x[n-m]*cmath.exp((-1j*2*math.pi*k*m)/window)

    for l in range(n,len(x)):
        #Xk[l] = cmath.exp((-1j*2*math.pi*k)/window)*(Xk[l-1]+x[l]-x[l-window]) #According to http://www.comm.toronto.edu/~dimitris/ece431/slidingdft.pdf
        Xk[l] = cmath.exp((-1j*2*math.pi*k)/window)*Xk[l-1]+x[l]-x[l-window] #According to http://www.cmlab.csie.ntu.edu.tw/DSPCourse/reference/Sliding%20DFT.pdf

    Xkmag = np.absolute(Xk)

    return Xkmag

def cross_correlation_detector(x,f0,time,show=False):

    t = np.arange(0,0.0001,0.000001)

    orig = np.sin(2*math.pi*f0*t)

    correlation = signal.correlate(x,orig)
    xplot = x + 2
    print(correlation.size)
    if show:
        #plot.plot(t,orig)
        plot.plot(time,correlation[:3000000],time,xplot)
        plot.show()

if __name__=='__main__':
    data = load_signal()
    #plot_data(data)
    #scipy_spectrogram(data,250000000)
    #peak_ind = detect_peaks(data[0,:]-np.average(data[0,:]), mph=None, mpd=4000, threshold=0, edge='rising',
    #             kpsh=False, valley=False, show=False, ax=None)
    #process_peaks(peak_ind)

    #detect_zero_crossings(data[0,:]-np.average(data[0,:]),show=True)
    kHz40 = sdft(data[0, :]-np.average(data[0, :]), 40000, 250*10**6, 25000, data[3, :], show=False)
    ind = detect_zero_crossings(kHz40 - 250, show=False)
    delay = data[3,ind[2]]
    distance = 1480 * delay
    print "Time delay: ",delay," Distance: ",distance
    cross_correlation_detector(data[0, :]-np.average(data[0, :]), 40000,  data[3, :], show=True)