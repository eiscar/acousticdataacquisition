#!/usr/bin/python
from __future__ import division, print_function
import serial
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import os

import signal_processing as sp


def readdata():
    BUFLEN = 5000

    num_data = 0
    adc_freq = 0
    circbuflen = 0
    record = False
    t = []
    data = []
    index = []
    lines = []
    ser = serial.Serial('/dev/ttyACM0', baudrate=115200)#, timeout=1)  # open serial port
    ser.reset_input_buffer()
    ser.flushInput()
    while 1:
        line = ser.readline().replace('\r', '')
        if record:
            if 'finished' in line:
                splitline = line.replace(' ', '').split(',')
                circbuflen = int(splitline[-3])
                BUFLEN = int(splitline[-2])
                adc_freq =int(splitline[-1])
                break
            else:
                lines.append(line)

        if "NB" in line:
            #print("HELP")
            #print("New buffer: "+line)
            lines = []
            record = True
            delay = float(line.split(':')[-1])
            adq_time = float(line.split(' ')[-5])
        if "Circular" in line or 'Trigger' in line:
            pass
            #print(line)

    for line in lines:
        try:
            splitline = line.replace(' ', '').split(',')
            index.append(int(splitline[0]))
            data.append(float(splitline[1])/(2**12))
            new_t = float(splitline[2])
            if len(t) > 0:
                if new_t < t[-1]:
                    new_t += 1
            t.append(new_t)
        except:
            print(line)
            #    num_data+=1
            #except ValueError:
            #    print("Value error with "+str(line)+" as line")
            #    pass
            #except IndexError as e:
            #    print(e, line)
    print(lines)
    ser.close()             # close port
    plt.plot(index)
    plt.show()
    return t, data, delay, num_data, adc_freq, circbuflen, BUFLEN


if __name__=="__main__":

    folder = '/home/eiscar/GDrive/AcousticPositioningSystem/Software/AcousticDataAcquisition' #'/Users/Eduardo/Codigo/Acoustics'
    print('Waiting for data')
    t, data, delay, numdata, adc_freq, circ_buf_len, buf_len = readdata()
    t = np.array(t)
    data = np.array(data)
    print('Adc freq: ',adc_freq,' Hz')
    print('Received buffer length:',buf_len)
    print('Received circular buffer length:',circ_buf_len)
    print('Time delay: ',delay,' s')
    numfiles = len(os.listdir(folder))
    fname = folder+'/data_'+ str(numfiles)+'_'+str(delay)+'.csv'
    np.savetxt(fname, data)
    np.savez(fname,data,adc_freq,buf_len,circ_buf_len,delay,numdata)
    data*=3.3
    #t= np.arange(delay,delay+(1/adc_freq)*buf_len,1/adc_freq)

    #peakind = sp.detect_peaks(data[circ_buf_len:numdata],show=False)
 #   ind_diff = np.ediff1d(peakind)
 #   period = ind_diff * (1 / adc_freq)
#    plt.plot(1 / period)

   # rfft_data = np.absolute(np.fft.rfft(data[:numdata]-np.mean(data[circ_buf_len:numdata])))
    #rfft_freq = np.fft.rfftfreq(data[circ_buf_len:numdata].size,1/adc_freq)
    #plt.plot(rfft_freq,rfft_data)

    plt.plot(t,data)
    plt.ylabel('Signal [V]')
    plt.ylim(0,3.3)
    plt.show()