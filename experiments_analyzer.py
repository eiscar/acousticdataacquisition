from __future__ import division, print_function
import numpy as np
import os
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import acoustic_utilities as au
import signal_processing as sp

if __name__=="__main__":
    folder = '/Users/Eduardo/Codigo/Acoustics/Trial3'
    filename = 'data_6_0.003541.csv.npz'
    fname = os.path.join(folder,filename)
    with open(fname) as file:
        loaded_data = np.load(file)
        data = loaded_data['arr_0']*3.3
        adc_freq = loaded_data['arr_1']
        buf_len = loaded_data['arr_2']
        circ_buf_len = loaded_data['arr_3']
        delay = loaded_data['arr_4']
        numdata = loaded_data['arr_5']

    print('Adc freq: ', adc_freq, ' Hz')
    print('Received buffer length:', buf_len)
    print('Received circular buffer length:', circ_buf_len)
    print('Time delay: ', delay, ' s')
    print('Distance delay: ', delay*au.speed_of_sound(15, 0, 101000), ' m')

    #SDFT detection:
    Xk = sp.sdft(data[:circ_buf_len],41700,254000,10)
    plt.plot(Xk)
    plt.show()

    rfft_data_signal = np.absolute(np.fft.rfft(data[circ_buf_len:numdata] - np.mean(data[circ_buf_len:numdata])))
    rfft_data_presignal = np.absolute(np.fft.rfft(data[:circ_buf_len] - np.mean(data[:circ_buf_len])))

    #rfft_data = rfft_data[range(n / 2)]
    rfft_freq_signal = np.fft.rfftfreq(data[circ_buf_len:numdata].size, 1 / adc_freq)
    rfft_freq_presignal = np.fft.rfftfreq(data[:circ_buf_len].size, (1 / adc_freq)*(350/250))

    f, axarr = plt.subplots(3)
    axarr[0].plot(data[:numdata])
    axarr[0].set_title('Received Signal')
    axarr[0].set_ylabel('Amplitude [V]')

    axarr[1].set_title('FFT after trigger')
    axarr[1].plot(rfft_freq_signal, rfft_data_signal)

    axarr[2].set_title('FFT before trigger')
    axarr[2].plot(rfft_freq_presignal, rfft_data_presignal)
    plt.show()